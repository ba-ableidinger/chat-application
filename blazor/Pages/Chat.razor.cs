using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ChatApplication.Blazor.Services;
using ChatApplication.Shared;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.JSInterop;

namespace ChatApplication.Blazor.Pages
{
    public partial class Chat
    {
        private string NewMessage { get; set; } = "";

        private List<Message> Messages { get; set; } = new List<Message>();

        private string UserName { get; set; }

        [Inject]
        private ChatHubService ChatHubService { get; set; }

        [Inject]
        private IJSRuntime JsRuntime { get; set; }

        protected override void OnInitialized()
        {
            ChatHubService.User.Subscribe(userName =>
            {
                UserName = userName;
                StateHasChanged();
            });

            ChatHubService.InitialMessagesSubject.Subscribe(async (messages) =>
            {
                if (messages != null)
                {
                    Messages = new List<Message>(messages);
                    StateHasChanged();
                    await JsRuntime.InvokeAsync<object>("scrollToBottom");
                }
            });

            ChatHubService.MessageSubject.Subscribe(async (message) =>
            {
                if (message != null)
                {
                    Messages.Add(message);
                    StateHasChanged();
                    await JsRuntime.InvokeAsync<object>("scrollToBottomIfStuck");
                }
            });
        }

        protected override async Task OnAfterRenderAsync(bool firstRender)
        {
            if (firstRender)
            {
                await JsRuntime.InvokeAsync<object>("initIntersectionObserver");
            }
        }

        private async Task OnMessageSend()
        {
            if (string.IsNullOrEmpty(NewMessage)) { return; }

            await ChatHubService.SendMessage(new Message { Body = NewMessage });

            Console.WriteLine("clearing");
            NewMessage = "";
            StateHasChanged();
        }

        private async Task HandleKeyUp(KeyboardEventArgs args)
        {
            if (args.Key == "Enter")
            {
                await OnMessageSend();
            }
        }
    }
}