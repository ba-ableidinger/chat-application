using ChatApplication.Shared;
using Microsoft.AspNetCore.Components;

namespace ChatApplication.Blazor.Shared.Components
{
    public partial class MessageView
    {
        [Parameter]
        public Message Message { get; set; }

        [Parameter]
        public bool IsOwn { get; set; } = false;

        [Parameter]
        public bool ShowUser { get; set; } = true;
    }
}