using System;
using System.Threading.Tasks;
using ChatApplication.Blazor.Services;
using Microsoft.AspNetCore.Components;

namespace ChatApplication.Blazor.Shared.Components
{
    public partial class User
    {
        [Inject]
        private ChatHubService ChatHubService { get; set; }

        private string UserName { get; set; }

        private bool IsEditing { get; set; } = false;

        protected override void OnInitialized()
        {
            ChatHubService.User.Subscribe(userName =>
            {
                UserName = userName;
                StateHasChanged();
            });
        }

        private void OnEditButtonClick()
        {
            IsEditing = !IsEditing;
            StateHasChanged();
        }

        private async Task OnEditDoneButtonClick()
        {
            IsEditing = false;
            await ChatHubService.SetUser(UserName);
            StateHasChanged();
        }
    }
}