using System;
using System.Collections.Generic;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using System.Threading.Tasks;
using Blazored.LocalStorage;
using ChatApplication.Shared;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;

namespace ChatApplication.Blazor.Services
{
    public class ChatHubService
    {
        public IObservable<string> User { get; private set; }
        private BehaviorSubject<string> UserSubject { get; set; }


        public BehaviorSubject<IEnumerable<Message>> InitialMessagesSubject { get; private set; }

        public BehaviorSubject<Message> MessageSubject { get; private set; }

        private ILocalStorageService LocalStorage;

        private HubConnection HubConnection { get; set; }

        public ChatHubService(ILocalStorageService localStorage)
        {
            LocalStorage = localStorage;

            UserSubject = new BehaviorSubject<string>(null);
            User = UserSubject.AsObservable();

            InitialMessagesSubject = new BehaviorSubject<IEnumerable<Message>>(null);

            MessageSubject = new BehaviorSubject<Message>(null);
        }

        public async Task Initialize(IConfiguration config)
        {
            UserSubject.OnNext(await LocalStorage.GetItemAsync<string>("user"));

            HubConnection = new HubConnectionBuilder()
                .WithUrl(new Uri($"{config["ApiUrl"]}/chatHub"))
                .Build();

            HubConnection.On<IEnumerable<Message>>("InitialMessages", (messages) =>
            {
                InitialMessagesSubject.OnNext(messages);
            });

            HubConnection.On<Message>("Message", (message) =>
            {
                MessageSubject.OnNext(message);
            });

            await HubConnection.StartAsync();
        }

        public async Task SendMessage(Message message)
        {
            if (HubConnection.State != HubConnectionState.Connected) { return; }

            message.User = UserSubject.Value;

            await HubConnection.SendAsync("SendMessage", message);
        }

        public async Task SetUser(string user)
        {
            await LocalStorage.SetItemAsync("user", user);
            UserSubject.OnNext(user);
        }

        public async Task ClearUser(string user)
        {
            await LocalStorage.SetItemAsync<string>("user", null);
            UserSubject.OnNext(null);
        }
    }
}