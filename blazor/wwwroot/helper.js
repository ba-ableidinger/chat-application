let isStuckToBottom = false;

function scrollToBottom() {
    const messagesEl = document.getElementById('messagesRef');
    messagesEl.scrollTop = messagesEl.scrollHeight;
}

function initIntersectionObserver() {
    const messagesBottomEl = document.getElementById('messagesBottomRef');

    const observer = new IntersectionObserver((entries) => {
        entries.forEach(entry => {
            isStuckToBottom = entry.isIntersecting;
        });
    }, {
        threshold: 0.01
    });

    observer.observe(messagesBottomEl);
}

function scrollToBottomIfStuck() {
    if (!isStuckToBottom) return;

    scrollToBottom();
}