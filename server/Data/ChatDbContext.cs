using ChatApplication.Shared;
using Microsoft.EntityFrameworkCore;

namespace ChatApplication.Server.Data
{
    public class ChatDbContext : DbContext
    {
        public DbSet<Message> Messages { get; set; }

        public ChatDbContext(DbContextOptions<ChatDbContext> options) : base(options) { }
    }
}