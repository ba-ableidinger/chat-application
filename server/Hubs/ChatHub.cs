using System;
using System.Threading.Tasks;
using ChatApplication.Server.Data;
using ChatApplication.Shared;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace ChatApplication.Server.Hubs
{

    [EnableCors("all")]
    public class ChatHub : Hub
    {

        private ChatDbContext DbContext { get; set; }

        public ChatHub(ChatDbContext context)
        {
            DbContext = context;
        }

        public override async Task OnConnectedAsync()
        {
            var messages = await DbContext.Messages.ToListAsync();

            await Clients.Caller.SendAsync("InitialMessages", messages);
        }

        public async Task SendMessage(Message message)
        {
            await DbContext.Messages.AddAsync(message);
            await DbContext.SaveChangesAsync();

            await Clients.All.SendAsync("Message", message);
        }
    }
}