import { Message } from './../../shared/models/message';
import { ChatHubService } from './../../shared/services/chat-hub.service';
import { Component, ElementRef, ViewChild, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { take, skip } from 'rxjs/operators';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit, AfterViewInit {

  public messages: Message[] = [];

  public messageControl = new FormControl(undefined);

  public isStuckToBottom = false;

  @ViewChild('messagesRef') messagesRef: ElementRef;
  @ViewChild('messagesBottomRef') messagesBottomRef: ElementRef;

  constructor(public chatHubService: ChatHubService, private changeDetector: ChangeDetectorRef) { }

  ngOnInit(): void {
    this.chatHubService.initialMessages$
      .pipe(skip(1), take(1))
      .subscribe((messages) => {
        if (messages) {
          this.messages = messages;
          this.changeDetector.detectChanges();
          this.messagesRef.nativeElement.scrollTop = this.messagesRef.nativeElement.scrollHeight;
        }
      });

    this.chatHubService.message$
      .subscribe((message) => {
        if (message) {
          this.messages.push(message);
          if (this.isStuckToBottom) {
            this.changeDetector.detectChanges();
            this.messagesRef.nativeElement.scrollTop = this.messagesRef.nativeElement.scrollHeight;
          }
        }
      });
  }

  ngAfterViewInit(): void {
    const observer = new IntersectionObserver((entries) => {
      entries.forEach(entry => {
        this.isStuckToBottom = entry.isIntersecting;
      });
    }, {
      threshold: 0.01
    });

    observer.observe(this.messagesBottomRef.nativeElement);
  }

  public sendMessage(): void {
    const message: Message = {
      body: this.messageControl.value,
      user: undefined
    };

    this.chatHubService.sendMessage(message);

    this.messageControl.reset();
  }

}
