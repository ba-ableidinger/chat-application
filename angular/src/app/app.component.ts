import { Message } from './../shared/models/message';
import { ChatHubService } from './../shared/services/chat-hub.service';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'ChatApplicationAngular';

  public userControl = new FormControl();
  public messageControl = new FormControl();

  constructor(private chatHubService: ChatHubService) { }

  public send(): void {
    const message: Message = {
      user: this.userControl.value,
      body: this.messageControl.value
    };

    this.chatHubService.sendMessage(message);
  }
}
