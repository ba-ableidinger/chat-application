import { ChatHubService } from './../../../shared/services/chat-hub.service';
import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent {

  public user$: Observable<string>;

  public userControl = new FormControl(undefined, [Validators.required]);

  public isEdit = false;

  constructor(private chatHubService: ChatHubService) {
    this.user$ = chatHubService.user$;

    this.user$.subscribe(user => this.userControl.patchValue(user));
  }

  public setUser(): void {
    this.chatHubService.setUser(this.userControl.value);
    if (this.isEdit) {
      this.isEdit = false;
    }
  }

  public editUser(): void {
    this.isEdit = true;
  }

}
