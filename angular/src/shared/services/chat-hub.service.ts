import { environment } from './../../environments/environment';
import { Message } from './../models/message';
import { Injectable } from '@angular/core';
import { HubConnectionBuilder, HubConnection, HubConnectionState } from '@microsoft/signalr';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ChatHubService {

  private connection: HubConnection;

  private userSubject = new BehaviorSubject<string>(undefined);
  public user$ = this.userSubject.asObservable();

  private initialMessagesSubject = new BehaviorSubject<Message[]>(undefined);
  public initialMessages$ = this.initialMessagesSubject.asObservable();

  private messageSubject = new BehaviorSubject<Message>(undefined);
  public message$ = this.messageSubject.asObservable();

  constructor() {
    this.init();
  }

  private init(): void {
    this.userSubject.next(localStorage.getItem('user'));

    this.connection = new HubConnectionBuilder()
      .withUrl(`${environment.apiUrl}/chatHub`)
      .build();

    this.connection.on('InitialMessages', (messages: Message[]) => {
      this.initialMessagesSubject.next(messages);
    });

    this.connection.on('Message', (message: Message) => {
      this.messageSubject.next(message);
    });

    this.connection.start();
  }

  public sendMessage(message: Message): void {
    if (this.connection.state !== HubConnectionState.Connected) { return; }

    message.user = this.userSubject.getValue();

    this.connection.send('SendMessage', message);
  }

  public clearUser(): void {
    localStorage.removeItem('user');
    this.userSubject.next(undefined);
  }

  public setUser(user: string): void {
    localStorage.setItem('user', user);
    this.userSubject.next(user);
  }
}
