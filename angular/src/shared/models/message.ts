export interface Message {
    id?: string;
    user: string;
    body: string;
    created?: Date;
}
