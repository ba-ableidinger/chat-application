using System;
using System.ComponentModel.DataAnnotations;

namespace ChatApplication.Shared
{
    public class Message
    {
        [Key]
        public Guid Id { get; set; }

        public string User { get; set; }

        public string Body { get; set; }

        public DateTime Created { get; set; } = DateTime.Now;
    }
}